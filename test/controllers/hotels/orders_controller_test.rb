  require "test_helper"

  class Hotels::OrdersControllerTest < ActionDispatch::IntegrationTest
    def setup
      @hotel = hotels(:hotel1)
      @order = orders(:order1)
    end

    test 'create' do
      assert_difference 'Order.count', 1 do
        post hotel_orders_path(@hotel)
      end
      assert_response :success
    end
  end
