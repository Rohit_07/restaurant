require "test_helper"

class Hotels::OrderItemsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @hotel = hotels(:hotel1)
  end

  test 'summary' do
    get hotel_summary_path(@hotel)
    assert_response :success
  end
end
