class Hotel < ApplicationRecord
  has_many :dishes, class_name: 'Dish', inverse_of: :hotel
  has_many :orders, class_name: 'Order', inverse_of: :hotel
  has_one_attached :profile_picture
  has_many_attached :ambience

  scope :active, -> do
    joins(:dishes)
      .where.not(dishes: { id: nil })
      .distinct('hotels.id')
  end
end