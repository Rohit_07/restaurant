require 'csv'

class DataParsing
  def initialize
  end

  def csv_file_path
    @csv_file_path ||= File.read("/Users/rohitgopinath/Downloads/chennai.csv")
  end

  def table
    CSV.parse(csv_file_path, headers: true, skip_blanks: true)
  end

  def import_data
    table.each do |row|
      next unless [row["Name of Restaurant"], row["Address"], row['Location'], row['Dining Rating'], row['Dining Rating Count'], row['Delivery Rating'], row['Delivery Rating Count']].all?
      hotel = Hotel.create(
        name:                  row["Name of Restaurant"],
        address:               row["Address"],
        location:              row['Location'],
        dining_rating:         row['Dining Rating'],
        dining_rating_count:   row['Dining Rating Count'],
        delivery_rating:       row['Delivery Rating'],
        delivery_rating_count: row['Delivery Rating Count'],
        cuisine:               YAML.load(row['Cuisine'])
      )
      next if YAML.load(row['Top Dishes']) == "Invalid"
      YAML.load(row['Top Dishes']).each do |dish|
        hotel.dishes.create(name: dish, price: (50..400).to_a.sample)
      end
    end
  end
end
