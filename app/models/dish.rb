class Dish < ApplicationRecord
  belongs_to :hotel,inverse_of: :dishes
  has_many :order_items,inverse_of: :dish
end