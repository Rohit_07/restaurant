class OrderItem < ApplicationRecord
  belongs_to :order,inverse_of: :order_items
  belongs_to :dish,inverse_of: :order_items

  def unit_price
    if persisted?
      self[:price]
    else
      dish.price
    end
  end
  def total
    unit_price.sum
  end
end
