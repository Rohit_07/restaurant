class HomeController < ApplicationController

  def index
    @pagy, @hotels = pagy(Hotel.active.order(:name), items: 60, page: params[:page] || 1)
  end

  def search
    @hotels = Hotel.where("name LIKE ?", "%" + params[:search] + "%")
  end

  def orders
    @orders = Order.order(created_at: :desc)
  end

end