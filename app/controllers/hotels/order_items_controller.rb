class Hotels::OrderItemsController < ApplicationController

  def index
    @order = Order.find_by_id params[:order_id]
    @item =@order.order_items.all
  end


  def summary
    @hotel = Hotel.find_by_id params[:hotel_id]
    @dishes  = @hotel.dishes.where(id: params[:ids])
  end

end