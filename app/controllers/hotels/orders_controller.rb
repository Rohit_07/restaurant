class Hotels::OrdersController < ApplicationController

  def index
    @order=Orders.order(created_at: :desc)
  end


  def create
    @hotel=Hotel.find_by_id params[:hotel_id]
    @order = @hotel.orders.create(params.permit(:name,:contact,:total))
    @dishes =@hotel.dishes.where(id: params[:dish_ids])
    @dishes.each do |t|
      t.order_items.create(order:@order)
    end
  end


end