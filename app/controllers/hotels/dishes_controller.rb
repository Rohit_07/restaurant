class Hotels::DishesController < ApplicationController

  def index
    @hotel  = Hotel.find_by_id params[:hotel_id]
    @dishes = @hotel.dishes.all
  end


end


