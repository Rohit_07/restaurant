class ApplicationController < ActionController::Base
  include Pagy::Backend

  def res_blob_path(blob, options = {})
    if Rails.env.production?
      path = Rails.application.routes.url_helpers.rails_blob_path(blob, options.merge(only_path: true))
      host = ApplicationRecord.current_blob_url
      "#{host}#{path}"
    else
      Rails.application.routes.url_helpers.rails_blob_path(blob, options.merge(only_path: true))
    end
  end

end
