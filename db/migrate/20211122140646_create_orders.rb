class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders , id: :uuid do |t|
      t.references :hotel, foreign_key: true, type: :uuid
      t.integer :total
      t.string :name
      t.integer :contact
      t.timestamps
    end
  end
end
