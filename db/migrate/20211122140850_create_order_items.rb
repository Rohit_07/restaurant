class CreateOrderItems < ActiveRecord::Migration[6.1]
  def change
    create_table :order_items, id: :uuid do |t|
      t.references :dish, foreign_key: true, type: :uuid
      t.references :order, foreign_key: true, type: :uuid
      t.timestamps
    end
  end
end
