class CreateHotelsAndDishes < ActiveRecord::Migration[6.1]
  def change
    create_table :hotels, id: :uuid do |t|
      t.string :name
      t.string :address
      t.string :location
      t.string :dining_rating
      t.integer :dining_rating_count
      t.string :delivery_rating
      t.integer :delivery_rating_count
      t.jsonb :cuisine, default: []
      t.timestamps
    end
    create_table :dishes, id: :uuid do |t|
      t.references :hotel, foreign_key: true, type: :uuid
      t.string :name
      t.string :price
      t.timestamps
    end
  end
end
