require 'csv'
table = CSV.parse(File.read('/Users/rohitgopinath/restaurant/lib/chennai.csv'), headers: true)

table.each do |row|
  hotel = Hotel.create(
    name:                  row["Name of Restaurant"],
    address:               row["Address"],
    location:              row['Location'],
    dining_rating:         row['Dining Rating'],
    dining_rating_count:   row['Dining Rating Count'],
    delivery_rating:       row['Delivery Rating'],
    delivery_rating_count: row['Delivery Rating Count'],

  )
  YAML.load(row['Top Dishes']).each do |dish|
    a=Dish.new(name:  dish,price: (50..400).to_a.sample,hotel_id: hotel.id)
    a.save
  end
end

