Rails.application.routes.draw do
  devise_for :users
  root 'home#index'
  resources :home, only: [:index]
  get '/orders', to:'home#orders'
  get '/search', to: "home#search"
  resources :hotels ,except: %i(edit new) do
    scope module: :hotels do
      resources :dishes, only: [:index]
      resources :order_items, only: [:index]
      get '/summary/', to: "order_items#summary"
      resources :orders, only: [:index,:create]
    end
  end
end
